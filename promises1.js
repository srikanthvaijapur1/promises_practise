// Problem 1: Write a function that will return a particular board's information based on the boardID that is passed from the given list of boards in boards.json and then pass control back to the code that called it by using a callback function.

const boards = require("./data/boards.json");

// const boardsId = (boards) => {
//     return new Promise((resolve, reject) => {
//         setTimeout(() => {
//             if (boards) {
//                 resolve(boards);
//             } else {
//                 reject("boards file not found");
//             }
//         }, 2000);
//     })
// };

// const getInfo = (boards, id) => {
//     return boards.filter((board) => {
//         if (board.id === id) {
//             return board
//         }
//     })

// }

// boardsId(boards)
//     .then((boards) => {
//         return getInfo(boards, "mcu453ed")
//     })
//     .then(data => console.log(data))

const boardsId = (boards, id) => {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            const res = boards.filter((board) => {
                if (board.id === id) {
                    return board
                }
            })
            if (res) {
                resolve(res)
            } else {
                reject("no id found")
            }
        })
    })
}

const getInfo = (data) => {
    if (data) {
        console.log(data)
    } else {
        console.error({
            msg: "data not found"
        })
    }

};



// boardsId(boards, "mcu453ed")
//     .then((data) => {
//         return getInfo(data)
//     });



module.exports = boardsId;


