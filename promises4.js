/* 
	Problem 4: Write a function that will use the previously written functions to get the following information. You do not need to pass control back to the code that called it.

    Get information from the Thanos boards
    Get all the lists for the Thanos board
    Get all cards for the Mind list simultaneously
*/

const boards = require("./data/boards.json");
const cards = require("./data/cards.json");
const lists = require("./data/lists.json")


const promise1 = require("./promises1");
const promise2 = require("./promises2");
const promise3 = require("./promises3");


promise1(boards, "mcu453ed")
    .then((data) => data.map(val => val.id))
    .then(id => promise2(lists, id))
    .then(data => {
        return data.find((val) => {
            if (val.name === 'Mind') {
                return val.id
            }
        })

    })
    .then(data => promise3(cards, data.id))
    .then(data => console.log(data))
    .catch((err) => console.error(err))