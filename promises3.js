// Problem 3: Write a function that will return all cards that belong to a particular list based on the listID that is passed to it from the given data in cards.json. Then pass control back to the code that called it by using a callback function.

const cards = require("./data/cards.json");

const cardId = (data, id) => {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            if (data[id]) {
                resolve(data[id])
            } else {
                reject(new Error("Id not found"));
            }
        }, 2000)
    })
};

const printId = (data) => {
    if (data) {
        console.log(data);
    } else {
        console.log({
            msg: "Data not found"
        });
    }
};

// cardId(cards, "qwsa221")
//     .then(data => printId(data))
//     .catch(error=>console.error(error));

module.exports = cardId;