/* 
	Problem 5: Write a function that will use the previously written functions to get the following information. You do not need to pass control back to the code that called it.

    Get information from the Thanos boards
    Get all the lists for the Thanos board
    Get all cards for the Mind and Space lists simultaneously
*/

const cards = require("./data/cards.json");
const boards = require("./data/boards.json");
const list = require("./data/lists.json");

const boardsId = require("./promises1");
const listsId = require("./promises2");
const cardId = require("./promises3");

boardsId(boards, "mcu453ed")
    .then(data => data[0].id)
    .then(id => listsId(list, id))
    .then(data => data.map(val => val.id))
    .then(data => data.map(val => (cardId(cards, val))))
    .then(data => console.log(data))
    .catch(err => console.error(err));