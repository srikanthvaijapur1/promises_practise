// Problem 2: Write a function that will return all lists that belong to a board based on the boardID that is passed to it from the given data in lists.json. Then pass control back to the code that called it by using a callback function.

const list = require("./data/lists.json");

const listId = (list, id) => {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            if (list[id]) {
                resolve(list[id])
            } else {
                reject("Id not found")
            }
        }, 2000)
    })
};

const getList = (data)=>{
    if(data){
        console.log(data)
    }else{
        console.error("data not found")
    }
}

// listId(list,'mcu453ed')
// .then((data)=>{
//     return getList(data)
// });

module.exports = listId;