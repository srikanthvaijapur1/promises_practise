function getSum(a, b) {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            if (Number.isInteger(a) && Number.isInteger(b)) {
                let sum = a + b;
                resolve(sum);
            } else {
                reject(new Error('Not Integer'));
            }

        }, 2000);
    });
}

function getDouble(num) {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            if (Number.isInteger(num)) {
                let doubleNum = num * 2;
                resolve(doubleNum);
            } else {
                reject(new Error('Number is not Integer'));
            }
        }, 1000)
    });
}


getSum(10, 10)
    .then((data) => {
        console.log(data)
    })
    .then((doubleNum) => {
        console.log(doubleNum)
        return getDouble(doubleNum)
    })
    .then((doubleNum) => console.log(doubleNum))
// })
// .then((doubleNum) => {
//     console.log(doubleNum);
// })
// .catch((err) => {
//     console.error(err);
// })

// let promise1 = getSum(10,10);
// let promise2 = getDouble(100);

// Promise.all([promise1,promise2]).then((arr) => {
//     let res1 = arr[0];
//     let res2 = arr[1];
//     console.log(res1);
//     console.log(res2);
// }).catch((err) => {
//     console.error(err);
// });